import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './router/auth/auth.guard';
import { AdminComponent } from './pages/admin/admin.component';
import { Role } from './services/auth/role.model';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'admin' },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/admin/collaborator/point' },
      { path: 'monitor', loadChildren: () => import('src/app/pages/admin/monitor/monitor.module').then(m => m.MonitorModule), data: { roles: [Role.Manager] } },
      { path: 'collaborator', loadChildren: () => import('src/app/pages/admin/collaborator/collaborator.module').then(m => m.CollaboratorModule) }
    ], 
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
