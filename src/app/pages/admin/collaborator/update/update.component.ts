import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { CollaboratorService } from 'src/app/services/collaborator/collaborator.service';
import { Collaborator } from 'src/app/services/collaborator/collaborator.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  form: FormGroup;

  collaborator$: Observable<Collaborator>;

  collaborator: Collaborator;

  isShowForm: boolean = true;

  success: boolean;

  error: boolean;

  constructor(public fb: FormBuilder,
    private route: ActivatedRoute,
    public collaboratorService: CollaboratorService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [{ value: '', disabled: true }, [Validators.required]],
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      role: ['', [Validators.required]],
    })

    var id = this.route.snapshot.params.id
    this.collaborator$ = this.collaboratorService.getById(id)
      .pipe(tap(collaborator => this.form.patchValue(collaborator || {})));
  }

  submitForm() {
    var id = this.route.snapshot.params.id;
    this.collaboratorService.update(id, this.form.getRawValue()).subscribe(res => {
      console.log('Collaborator updated!', res);
      this.collaborator = res;
      this.isShowForm = false;
      this.success = true;
    }, () => {
      this.resetForm();
      this.isShowForm = false;
      this.error = true;
    });
  }

  resetForm() {
    this.form.reset();
    this.collaborator$ = null;
    this.collaborator = null;
    this.error = false;
    this.success = false;
    this.isShowForm = true;
  }
}
