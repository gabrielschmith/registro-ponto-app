import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';

import { CollaboratorRoutingModule } from './collaborator-routing.module';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { HomeComponent } from './home/home.component';
import { PointComponent } from './point/point.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzTableModule, 
    NzSpaceModule,
    NzLayoutModule,
    NzGridModule,
    NzCardModule,
    NzDividerModule,
    NzButtonModule,
    NzFormModule,
    NzRadioModule,
    NzInputModule,
    NzIconModule,
    NzResultModule,
    NzSelectModule,
    NzDatePickerModule,
    NzDrawerModule,
    CollaboratorRoutingModule
  ],
  declarations: [CreateComponent, UpdateComponent, HomeComponent, PointComponent]
})
export class CollaboratorModule { }
