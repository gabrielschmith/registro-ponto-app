import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CollaboratorService } from 'src/app/services/collaborator/collaborator.service';
import { Collaborator } from 'src/app/services/collaborator/collaborator.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss']
})
export class PointComponent implements OnInit {

  form: FormGroup;
  
  optionList: Collaborator[] = [];

  isShowForm: boolean = true;

  success: boolean;

  error: boolean;

  isLoading: boolean;

  constructor(public fb: FormBuilder,
    public collaboratorService: CollaboratorService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      collaborator: [null, [Validators.required]],
      pointRecordDate: [new Date(), [Validators.required]],
      operation: ['', [Validators.required]],
    });

    this.collaboratorService.getAll().subscribe(res => {
      this.optionList = res;
      this.isLoading = false;
    })
  }

  submitForm() {
    const id = this.form.controls["collaborator"].value;
    this.collaboratorService.createPointRecord(id, this.form.value).subscribe(res => {
      console.log("Collaborator Point Record Created!", res)
      
      this.isShowForm = false;
      this.success = true;
    }, () => {
      this.resetForm();
      this.isShowForm = false;
      this.error = true;
    });
  }

  resetForm() {
    this.form.reset();
    this.error = false;
    this.success = false;
    this.isShowForm = true;
  }
}
