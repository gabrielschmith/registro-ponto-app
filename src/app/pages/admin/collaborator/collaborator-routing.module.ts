import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { PointComponent } from './point/point.component';
import { Role } from 'src/app/services/auth/role.model';

const routes: Routes = [
  { path: '', redirectTo: 'point', pathMatch: 'full'},
  { path: 'view', component: HomeComponent, data: { roles: [Role.Manager] }},
  { path: 'create', component: CreateComponent, data: { roles: [Role.Manager] } },
  { path: 'update/:id', component: UpdateComponent, data: { roles: [Role.Manager] } },
  { path: 'point', component: PointComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaboratorRoutingModule { }
