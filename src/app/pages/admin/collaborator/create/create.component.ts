import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CollaboratorService } from 'src/app/services/collaborator/collaborator.service';
import { Collaborator } from 'src/app/services/collaborator/collaborator.model';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form: FormGroup;

  collaborator: Collaborator;

  isShowForm: boolean = true;

  success: boolean;

  error: boolean;

  constructor(public fb: FormBuilder,
    public collaboratorService: CollaboratorService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      role: ['', [Validators.required]],
    })
  }

  submitForm() {
    this.collaboratorService.create(this.form.value).subscribe(res => {
      console.log("Collaborator Created!", this.collaborator)
      
      this.collaborator = res;
      this.isShowForm = false;
      this.success = true;
    }, () => {
      this.resetForm();
      this.isShowForm = false;
      this.error = true;
    });
  }

  resetForm() {
    this.form.reset();
    this.collaborator = null;
    this.error = false;
    this.success = false;
    this.isShowForm = true;
  }
}
