import { Component, OnInit } from '@angular/core';
import { CollaboratorService } from 'src/app/services/collaborator/collaborator.service';
import { Collaborator, CollaboratorPointRecord } from 'src/app/services/collaborator/collaborator.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listOfData: Collaborator[] = [];

  currentCollaborator: Collaborator;

  pointRecordDrawerVisible: boolean;
  
  constructor(public collaboratorService: CollaboratorService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.collaboratorService.getAll().subscribe((data: Collaborator[]) => {
      console.log(data);
      this.listOfData = data;
    });
  }

  delete(id: string) {
    this.collaboratorService.delete(id).subscribe(() => {
      this.load();
    });
  }

  open(collaborator: Collaborator): void {
    this.pointRecordDrawerVisible = true;
    this.currentCollaborator = collaborator;
  }

  close(): void {
    this.pointRecordDrawerVisible = false;
  }
}
