import { Component, OnInit } from '@angular/core';
import { CollaboratorService } from 'src/app/services/collaborator/collaborator.service';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit {

  totalCollaborators: number = 0;

  totalCollaboratorsHours: number = 0;

  constructor(private collaboratorService: CollaboratorService) { }

  ngOnInit() {
    this.collaboratorService.count().subscribe(r => this.totalCollaborators = r);
  }
}
