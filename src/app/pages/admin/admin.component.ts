import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Viewer } from 'src/app/services/auth/viewer.model';
import { Router } from '@angular/router';
import { Role } from 'src/app/services/auth/role.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  isCollapsed: boolean = false;

  title: string = "Registro Ponto - Stefanini";

  currentViewer: Viewer;

  constructor(
    private router: Router,
    private authenticationService: AuthService) {
    this.currentViewer = authenticationService.currentViewerValue.viewer;
  }

  ngOnInit(): void {
  }

  isManager(): boolean {
    return this.currentViewer.role == Role.Manager;
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
