import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, registerLocaleData } from "@angular/common";
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './pages/admin/admin.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import pt from '@angular/common/locales/pt';
import { NZ_I18N, pt_BR } from 'ng-zorro-antd/i18n';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { IconsProviderModule } from './icons-provider.module';
import { AuthInterceptor } from './router/auth/auth.interceptor';
import { AuthJwtInterceptor } from './router/auth/auth-jwt.interceptor';

registerLocaleData(pt);

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    NzMenuModule,
    NzDropDownModule,
    IconsProviderModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: pt_BR },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthJwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
