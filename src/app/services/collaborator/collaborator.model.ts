export interface Collaborator {
    id: string;
    name: string;
    username: string;
    password: string;
    role: string;
    collaboratorPointRecords: CollaboratorPointRecord[]
}

export interface CollaboratorPointRecord {
    pointRecordDate: Date;
    operation: string;
}