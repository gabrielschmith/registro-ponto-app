import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Collaborator, CollaboratorPointRecord } from './collaborator.model';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  create(collaborator: Collaborator): Observable<Collaborator> {
    return this.httpClient.post<Collaborator>(`${environment.apiUrl}/Collaborator`, JSON.stringify(collaborator), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  createPointRecord(id: string, collaboratorPointRecord: CollaboratorPointRecord): Observable<CollaboratorPointRecord> {
    return this.httpClient.post<CollaboratorPointRecord>(`${environment.apiUrl}/Collaborator/${id}/pointRecord`, JSON.stringify(collaboratorPointRecord), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  getById(id: string): Observable<Collaborator> {
    return this.httpClient.get<Collaborator>(`${environment.apiUrl}/Collaborator/${id}`)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  getAll(): Observable<Collaborator[]> {
    return this.httpClient.get<Collaborator[]>(`${environment.apiUrl}/Collaborator`)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  update(id: string, collaborator: Collaborator): Observable<Collaborator> {
    return this.httpClient.put<Collaborator>(`${environment.apiUrl}/Collaborator`, JSON.stringify(collaborator), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  delete(id: string) {
    return this.httpClient.delete<Collaborator>(`${environment.apiUrl}/Collaborator/${id}`, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  count() {
    return this.httpClient.get<number>(`${environment.apiUrl}/Collaborator/count`, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      )
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
